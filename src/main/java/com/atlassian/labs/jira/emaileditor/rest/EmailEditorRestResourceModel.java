package com.atlassian.labs.jira.emaileditor.rest;

import javax.xml.bind.annotation.*;
@XmlRootElement(name = "template")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmailEditorRestResourceModel {

    @XmlElement(name = "name")
    private String filename;

    @XmlElement(name = "contents")
    private String contents;

    @XmlElement(name = "type")
    private String type;

    public EmailEditorRestResourceModel() {
    }

    public EmailEditorRestResourceModel(String filename, String contents, String type) {
        this.filename = filename;
        this.contents = contents;
        this.type = type;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}