package com.atlassian.labs.jira.emaileditor.jira.webwork;

import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.util.ClassLoaderUtils;

import java.io.File;
import java.util.ArrayList;

@WebSudoRequired
public class EmailEditorAction extends JiraWebActionSupport {
    private static final Logger log = LoggerFactory.getLogger(EmailEditorAction.class);
    private ArrayList<String> textTemplates = new ArrayList<String>();
    private ArrayList<String> htmlTemplates = new ArrayList<String>();
    private ArrayList<String> subjectTemplates = new ArrayList<String>();

    public EmailEditorAction() {
        try {
            File folder = new File(ClassLoaderUtils.getResource("templates/email/text", this.getClass()).getFile());
            //System.getProperty("user.dir") + "/webapps/jira/WEB-INF/classes/templates/email/text/");
            File[] listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    textTemplates.add(listOfFiles[i].getName());
                }
            }
            folder = new File(ClassLoaderUtils.getResource("templates/email/html", this.getClass()).getFile());
            listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    htmlTemplates.add(listOfFiles[i].getName());
                }
            }
            folder = new File(ClassLoaderUtils.getResource("templates/email/subject", this.getClass()).getFile());
            listOfFiles = folder.listFiles();

            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    subjectTemplates.add(listOfFiles[i].getName());
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    public ArrayList<String> getTextTemplates() {
        return textTemplates;
    }

    public void setTextTemplates(ArrayList<String> textTemplates) {
        this.textTemplates = textTemplates;
    }

    @Override
    @RequiresXsrfCheck
    public String execute() throws Exception {
        return super.execute(); //returns SUCCESS
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        return super.doExecute();
    }

    @Override
    @RequiresXsrfCheck
    public String doDefault() throws Exception {
        return super.doDefault();
    }

    public ArrayList<String> getHtmlTemplates() {
        return htmlTemplates;
    }

    public void setHtmlTemplates(ArrayList<String> htmlTemplates) {
        this.htmlTemplates = htmlTemplates;
    }
    
    public ArrayList<String> getSubjectTemplates() {
        return subjectTemplates;
    }

    public void setSubjectTemplates(ArrayList<String> subjectTemplates) {
        this.subjectTemplates = subjectTemplates;
    }
}
